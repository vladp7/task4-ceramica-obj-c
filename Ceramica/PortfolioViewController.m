//
//  PortfolioViewController.m
//  Ceramica
//
//  Created by Владислав on 22.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "PortfolioViewController.h"
#import "PortfolioCollectionViewCell.h"
#import "PortfolioMenuItem.h"
#import "PortfolioMenuItems.h"

@interface PortfolioViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation PortfolioViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    PortfolioMenuItems* items = [[PortfolioMenuItems alloc] init];
    self.menuItemsArray = items.itemsArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.menuItemsArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PortfolioCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    aCell.headerLabel.text = [[self.menuItemsArray objectAtIndex:indexPath.row] header];
    aCell.materials.text = [[self.menuItemsArray objectAtIndex:indexPath.row] materials];
    aCell.image.image = [UIImage imageNamed:[[self.menuItemsArray objectAtIndex:indexPath.row] imageName]];
    
    return aCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
        [self performSegueWithIdentifier:@"showPortfolioItemSegue" sender:self];
}



-(CGSize) collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(self.view.frame.size.width, 175.0);
    //    if (indexPath.row == 0) {
    //        size = CGSizeMake(self.view.frame.size.width, 245.0);
    //    }
    //return self.view.frame.size;
    return size;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showPortfolioItemSegue"]) {
        
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
        
        PortfolioItemViewController *pivc;
        pivc = [segue destinationViewController];
        
        if ([[self.menuItemsArray objectAtIndex:indexPath.row] bigImage] != NULL) {
            pivc.imageName = [[self.menuItemsArray objectAtIndex:indexPath.row] bigImage];
        } else {
            pivc.imageName = [[self.menuItemsArray objectAtIndex:indexPath.row] imageName];
        }
        pivc.indexPathRow  = indexPath.row;
    }
}

@end
