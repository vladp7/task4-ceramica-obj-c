//
//  MaterialViewController.m
//  Ceramica
//
//  Created by Владислав on 29.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "MaterialViewController.h"
#import "MaterialHeaderTableViewCell.h"
#import "MaterialTableViewCell.h"
#import "Material.h"
#import "MaterialCollectionViewCell.h"
#import "Collection.h"

@interface MaterialViewController () <UITableViewDataSource, UITableViewDelegate> {
    
    __weak IBOutlet UITableView *mainTableView;
}

@end

@implementation MaterialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    mainTableView.dataSource = self;
    mainTableView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1 + [self.material.collectionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        MaterialHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
        
        if (cell == nil) {
            cell = [[MaterialHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HeaderCell"];
        }
        
        UIImage *image = [UIImage imageNamed:self.imageName];
        cell.headerImageView.image = image;
        
        cell.headerLabel.text = self.material.materialName;
        
        return cell;
    } else {
        MaterialTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MaterialCell"];
        
        if (cell == nil) {
            cell = [[MaterialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MaterialCell"];
        }

        Collection* collection = [self.material.collectionsArray objectAtIndex:indexPath.row - 1];
        cell.collectionLabel.text = collection.collectionName;
        
//        cell.collectionLabel.text = @"COLLECTION LABEL TEXT";
        
        return cell;
    }    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 170;
    } else {
        return 200;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
