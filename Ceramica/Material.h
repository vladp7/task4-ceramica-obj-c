//
//  Material.h
//  Ceramica
//
//  Created by Владислав on 27.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Material : NSObject

@property (strong, nonatomic) NSString* materialName;
@property (strong, nonatomic) NSString* imageName;
@property (strong, nonatomic) NSArray* collectionsArray;

@end
