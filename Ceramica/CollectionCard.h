//
//  CollectionCard.h
//  Ceramica
//
//  Created by Владислав on 30.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionCard : NSObject

@property (strong, nonatomic) NSString* collectionCardName;
@property (strong, nonatomic) NSString* collectionCardSize;
@property (strong, nonatomic) NSString* collectionCardImageName;

@end
