//
//  ViewController.m
//  Ceramica
//
//  Created by Владислав on 09.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.menuArray = @[@"Каталог", @"Портфолио", @"Декоративные элементы", @"Контакты"];
    self.menuImageArray = @[@"Katalog", @"Portfolio", @"Decoration", @"Contacts"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.menuArray count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *SimpleIdentifier = @"SimpleIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleIdentifier];
    }
    
    UIImage *image = [UIImage imageNamed:self.menuImageArray[indexPath.row]];
    cell.imageView.image = image;
    
    cell.textLabel.text = self.menuArray[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"showSegue" sender:self];
    }
}

@end
