//
//  PortfolioContainerTableViewCell.h
//  Ceramica
//
//  Created by Владислав on 28.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioContainerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *materialLabel;
@property (weak, nonatomic) IBOutlet UIImageView *materialImageView;
@property (weak, nonatomic) IBOutlet UILabel *materialsUsedLabel;

//@property (copy, nonatomic) NSString *material;
//@property (copy, nonatomic) NSString *imageName;

@end
