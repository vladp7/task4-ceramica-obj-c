//
//  MaterialCollectionViewCell.h
//  Ceramica
//
//  Created by Владислав on 29.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaterialCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *collectionImage;
@property (weak, nonatomic) IBOutlet UILabel *collectionItemLabel;


@end
