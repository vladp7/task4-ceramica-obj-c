//
//  MaterialHeaderTableViewCell.h
//  Ceramica
//
//  Created by Владислав on 29.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaterialHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
