//
//  PortfolioContainerViewController.m
//  Ceramica
//
//  Created by Владислав on 26.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "PortfolioContainerViewController.h"
#import "PortfolioItemViewController.h"
#import "PortfolioMenuItems.h"
#import "MaterialViewController.h"
#import "MaterialTableViewCell.h"
#import "Collection.h"
#import <QuartzCore/QuartzCore.h>

#import "Material.h"

@interface PortfolioContainerViewController () <UITableViewDataSource, UITableViewDelegate> {
    __weak IBOutlet UITableView *materialsTableView;
}
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@end

@implementation PortfolioContainerViewController {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.layer.cornerRadius = 5;
    self.mainView.layer.masksToBounds = YES;
    
    self.headerLabel.text = self.header;
    self.textTextView.text = self.textOfItem;
    
    
//    CGFloat tableViewHeight = materialsTableView.contentSize.height;
//    CGFloat heightForRow = tableViewWidth / 2.f + 15.f;
    
//    self.tableViewHeight.constant = tableViewHeight;
}

- (void)viewWillAppear:(BOOL)animated {
    
    CGFloat tableViewWidth = materialsTableView.contentSize.width;
    CGFloat heightForRow = tableViewWidth / 2.f + 15.f;
//    materialsTableView.
    self.tableViewHeight.constant = heightForRow * [self.materialsArray count];
}

- (IBAction)onClose:(id)sender {
    [self.delegate portfolioContainerCloseRequest:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.materialsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *SimpleIdentifier = @"SimpleIdentifier";
    
    PortfolioContainerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PortfolioContainerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleIdentifier];
    }
    
    Material * mat = [self.materialsArray objectAtIndex:indexPath.row];
    
    UIImage *image = [UIImage imageNamed:mat.imageName];
    cell.materialImageView.image = image;
    
    cell.materialLabel.text = mat.materialName;
    cell.materialsUsedLabel.text = [self.materialsUsed objectAtIndex:indexPath.row];
    
//    [materialsTableView sizeToFit];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat tableViewWidth = tableView.contentSize.width;
    CGFloat heightForRow = tableViewWidth / 2.f + 15.f;
    
    return heightForRow;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showMaterial" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showMaterial"]) {
        
        MaterialViewController *mvc;
        MaterialTableViewCell *mtvc;
        
        NSIndexPath *path = [materialsTableView indexPathForSelectedRow];
        
        mvc = [segue destinationViewController];
    
        mvc.material = [self.materialsArray objectAtIndex:path.row];
        mvc.imageName = self.imageName;
        
        Material* material = [self.materialsArray objectAtIndex:path.row];
        mtvc.collections = [material collectionsArray];
        
        NSLog(@"coount1 %d", [mtvc.collections count]);
        NSLog(@"coount2 %d", [[material collectionsArray] count]);
    }
    
}

@end
