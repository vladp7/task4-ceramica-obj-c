//
//  MaterialTableViewCell.m
//  Ceramica
//
//  Created by Владислав on 29.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "MaterialTableViewCell.h"
#import "MaterialCollectionViewCell.h"

@implementation MaterialTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//    NSLog(@"coooount %d", [self.collections count]);
    return 20;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MaterialCollectionViewCell *cCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    
    cCell.collectionImage.image = [UIImage imageNamed:@"Card Background"];
    cCell.collectionItemLabel.text = @"BL 01";
    
    //    cCell.headerLabel.text = [[self.dataArray objectAtIndex:indexPath.row] header];
    //    cCell.materials.text = [[self.dataArray objectAtIndex:indexPath.row] materials];
    //    cCell.image.image = [UIImage imageNamed:[[self.dataArray objectAtIndex:indexPath.row] imageName]];
    
    return cCell;
}

@end
