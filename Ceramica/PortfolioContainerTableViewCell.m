//
//  PortfolioContainerTableViewCell.m
//  Ceramica
//
//  Created by Владислав on 28.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "PortfolioContainerTableViewCell.h"

@implementation PortfolioContainerTableViewCell {
    UILabel *materialValue;
    UIImage *imageNameValue;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.materialsUsedLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.materialsUsedLabel.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1].CGColor;
    self.materialsUsedLabel.layer.borderWidth = 1.0;
    self.materialsUsedLabel.layer.cornerRadius = 12;
    
    
//    CGFloat tableViewWidth = tableView.contentSize.width;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
