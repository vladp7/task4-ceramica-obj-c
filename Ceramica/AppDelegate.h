//
//  AppDelegate.h
//  Ceramica
//
//  Created by Владислав on 09.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

