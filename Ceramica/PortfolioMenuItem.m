//
//  PortfolioMenuItem.m
//  Ceramica
//
//  Created by Владислав on 20.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "PortfolioMenuItem.h"

@implementation PortfolioMenuItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.header = @"no header";
        self.materials = @"no materials";
        self.imageName = @"no image";
    }
    return self;
}

@end
