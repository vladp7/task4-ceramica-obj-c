//
//  Collection.h
//  Ceramica
//
//  Created by Владислав on 30.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Collection : NSObject

@property (strong, nonatomic) NSString* collectionName;
@property (strong, nonatomic) NSArray* collectionCardsArray;

@end
