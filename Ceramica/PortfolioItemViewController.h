//
//  PortfolioItemViewController.h
//  Ceramica
//
//  Created by Владислав on 23.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortfolioContainerViewController.h"

@interface PortfolioItemViewController : UIViewController <PortfolioContainerViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *portfolioItemImage;

@property (weak, nonatomic) IBOutlet UIView *portfolioContainerView;

@property (strong, nonatomic) NSString* imageName;

@property (assign, nonatomic) NSUInteger indexPathRow;

@end
