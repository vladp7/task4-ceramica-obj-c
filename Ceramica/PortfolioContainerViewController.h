//
//  PortfolioContainerViewController.h
//  Ceramica
//
//  Created by Владислав on 26.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortfolioContainerTableViewCell.h"

@class PortfolioContainerViewController;
@protocol PortfolioContainerViewControllerDelegate <NSObject>

-(void)portfolioContainerCloseRequest:(PortfolioContainerViewController*)controller;

@end

@interface PortfolioContainerViewController : UIViewController

@property (nonatomic,weak) id<PortfolioContainerViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UITextView *textTextView;


@property (strong, nonatomic) NSString* header;
@property (strong, nonatomic) NSString* textOfItem;
@property (strong, nonatomic) NSString* imageName;

@property (strong, nonatomic) NSArray * dataArray;
@property (strong, nonatomic) NSArray * materialsArray;
@property (strong, nonatomic) NSArray * materialsUsed;

@end
