//
//  MaterialTableViewCell.h
//  Ceramica
//
//  Created by Владислав on 29.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaterialTableViewCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *collectionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;

@property (strong, nonatomic) NSArray * collections;


@end
