//
//  Material.m
//  Ceramica
//
//  Created by Владислав on 27.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "Material.h"

@implementation Material

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.materialName = @"no material";
        self.imageName = @"no image";
    }
    return self;
}

@end
