//
//  PortfolioCollectionViewCell.h
//  Ceramica
//
//  Created by Владислав on 23.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *materials;
@property (weak, nonatomic) IBOutlet UIImageView *image;


@end
