//
//  MaterialViewController.h
//  Ceramica
//
//  Created by Владислав on 29.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Material.h"

@interface MaterialViewController : UIViewController

@property (strong, nonatomic) Material * material;
@property (strong, nonatomic) NSString* imageName;

@end
