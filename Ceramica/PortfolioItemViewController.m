//
//  PortfolioItemViewController.m
//  Ceramica
//
//  Created by Владислав on 23.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "PortfolioItemViewController.h"
#import "PortfolioContainerViewController.h"
#import "PortfolioMenuItems.h"
#import "PortfolioMenuItem.h"
#import "Material.h"

@interface PortfolioItemViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *portfolioContainerTop;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end

@implementation PortfolioItemViewController

- (IBAction)learnMoreButton:(id)sender {
    [self slideContainerUp];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.portfolioItemImage.image = [UIImage imageNamed:self.imageName];
    
//    [self updateConstraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)slideContainerDown {
    [self slideContainerDown:(YES)];
}

-(void)slideContainerUp {
    [self slideContainerDown:(NO)];
}

-(void)slideContainerDown:(BOOL)down {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        
        float offset = self.mainView.bounds.size.height * 0.95;
        
        if (down == YES) {
            self.portfolioContainerTop.constant = offset;
        } else {
            self.portfolioContainerTop.constant = -offset;
        }
        
        [self.view layoutIfNeeded];
    }];
}

- (void)updateConstraints {
    float offset = self.mainView.bounds.size.height * 0.95;
    self.portfolioContainerTop.constant = -offset;
}

/*
#pragma mark - Navigation
*/
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showContainer"]) {
        PortfolioContainerViewController*vc = segue.destinationViewController;
        vc.delegate = self;
        
        PortfolioMenuItems*items = [[PortfolioMenuItems alloc] init];
        
        vc.header = [[items.itemsArray objectAtIndex:self.indexPathRow] header];
        
        vc.textOfItem = [[items.itemsArray objectAtIndex:self.indexPathRow] textOfItem];
        
        PortfolioMenuItem * item = [items.itemsArray objectAtIndex:self.indexPathRow];
        NSArray * materials = [item materialsArray];
        NSArray * materialsUsed = [item materialsUsed];
//        Material * mat = [[item materialsArray] objectAtIndex:2];
//        NSLog(@"Material: %@", [mat material]);
        
        vc.materialsArray = materials;
        vc.materialsUsed = materialsUsed;
        vc.imageName = self.imageName;
    }
}

#pragma mark - PortfolioContainerViewControllerDelegate

-(void)portfolioContainerCloseRequest:(PortfolioContainerViewController*)controller {
    [self slideContainerDown];
}
@end
