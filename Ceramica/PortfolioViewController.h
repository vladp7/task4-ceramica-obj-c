//
//  PortfolioViewController.h
//  Ceramica
//
//  Created by Владислав on 22.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "ViewController.h"
#import "PortfolioItemViewController.h"

@interface PortfolioViewController : ViewController 

//@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
//@property (weak, nonatomic) IBOutlet UILabel *materials;
//@property (weak, nonatomic) IBOutlet UIImageView *image;


@property (strong, nonatomic) NSArray * menuItemsArray;

@end
