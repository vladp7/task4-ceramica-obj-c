//
//  ViewController.h
//  Ceramica
//
//  Created by Владислав on 09.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (copy, nonatomic) NSArray *menuArray;
@property (copy, nonatomic) NSArray *menuImageArray;

@end

