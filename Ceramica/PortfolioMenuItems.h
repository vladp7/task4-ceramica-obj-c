//
//  PortfolioMenuItems.h
//  Ceramica
//
//  Created by Владислав on 27.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PortfolioMenuItems : NSObject

- (NSArray*) itemsArray;

@end
