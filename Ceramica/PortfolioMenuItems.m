//
//  PortfolioMenuItems.m
//  Ceramica
//
//  Created by Владислав on 27.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "PortfolioMenuItems.h"
#import "PortfolioMenuItem.h"
#import "Material.h"
#import "Collection.h"
#import "CollectionCard.h"

@implementation PortfolioMenuItems

- (NSArray*) itemsArray {
    
    CollectionCard* bl01 = [[CollectionCard alloc] init];
    CollectionCard* bl02 = [[CollectionCard alloc] init];
    CollectionCard* bl03 = [[CollectionCard alloc] init];
    
    CollectionCard* cp01 = [[CollectionCard alloc] init];
    CollectionCard* cp02 = [[CollectionCard alloc] init];
    CollectionCard* cp03 = [[CollectionCard alloc] init];
    
    CollectionCard* testCollectionCardStone = [[CollectionCard alloc] init];
    
    CollectionCard* testCollectionCardWood = [[CollectionCard alloc] init];
    
    
    Collection* boleroMarble = [[Collection alloc] init];
    Collection* capriMarble = [[Collection alloc] init];
    
    Collection* testCollectionStone = [[Collection alloc] init];
    
    Collection* testCollectionWood = [[Collection alloc] init];
    
    
    Material* marble = [[Material alloc] init];
    Material* stone = [[Material alloc] init];
    Material* wood = [[Material alloc] init];
    
    
    PortfolioMenuItem* item1 = [[PortfolioMenuItem alloc] init];
    PortfolioMenuItem* item2 = [[PortfolioMenuItem alloc] init];
    PortfolioMenuItem* item3 = [[PortfolioMenuItem alloc] init];
    PortfolioMenuItem* item4 = [[PortfolioMenuItem alloc] init];
    PortfolioMenuItem* item5 = [[PortfolioMenuItem alloc] init];
    PortfolioMenuItem* item6 = [[PortfolioMenuItem alloc] init];
    
    
    
    
    bl01.collectionCardName = @"BL 01";
    bl01.collectionCardSize = @"30x30";
    bl01.collectionCardImageName = @"Card Background";
    
    bl02.collectionCardName = @"BL 02";
    bl02.collectionCardSize = @"30x30";
    bl02.collectionCardImageName = @"Card Background2";
    
    bl03.collectionCardName = @"BL 03";
    bl03.collectionCardSize = @"30x30";
    bl03.collectionCardImageName = @"Card Background";
    
    cp01.collectionCardName = @"CP 01";
    cp01.collectionCardSize = @"30x30";
    cp01.collectionCardImageName = @"Card Background4";
    
    cp02.collectionCardName = @"CP 02";
    cp02.collectionCardSize = @"30x30";
    cp02.collectionCardImageName = @"Card Background5";
    
    cp03.collectionCardName = @"CP 03";
    cp03.collectionCardSize = @"30x30";
    cp03.collectionCardImageName = @"Card Background";
    
    
    boleroMarble.collectionName = @"BOLERO";
    boleroMarble.collectionCardsArray  = [NSArray arrayWithObjects:bl01, bl02, bl03, nil];
    
    capriMarble.collectionName = @"CAPRI";
    capriMarble.collectionCardsArray  = [NSArray arrayWithObjects:cp01, cp02, cp03, nil];
    
    testCollectionStone.collectionName = @"TEST COLLECTION STONE";
    testCollectionStone.collectionCardsArray  = [NSArray arrayWithObjects:testCollectionCardStone, nil];
    
    testCollectionWood.collectionName = @"TEST COLLECTION WOOD";
    testCollectionWood.collectionCardsArray  = [NSArray arrayWithObjects:testCollectionCardWood, nil];
    
    
    marble.materialName = @"Мрамор";
    marble.imageName = @"marble";
    marble.collectionsArray = [NSArray arrayWithObjects:boleroMarble, capriMarble, nil];
    
    stone.materialName = @"Камень";
    stone.imageName = @"Kamen";
    stone.collectionsArray = [NSArray arrayWithObjects:testCollectionStone, nil];
    
    wood.materialName = @"Дерево";
    wood.imageName = @"wood";
    wood.collectionsArray = [NSArray arrayWithObjects:testCollectionWood, nil];

    
    item1.header = @"Переговорная в офисе";
    item1.materials = @"Мрамор | Камень | Дерево";
    item1.imageName = @"Peregovornaya";
    item1.bigImage = @"Big photo";
    item1.textOfItem = @"Керамический гранит из коллекции Bolero спокойного, гармоничного рисунка с включениями слюды, которая поблескивает на свету, подойдет для любого интерьера благодаря классическому рисунку и мягким пастельным тонам. Обладая фактурой, максимально приближенной к бургундскому камню, плитка подчеркнет досто-инство английского классического стиля в вашем доме. Коллекция доступна в четырех вариантах цветового исполнения: светло-бежевый, серый, светло-коричневый, коричневый и трех основных форматах.  ";
    item1.materialsArray = [NSArray arrayWithObjects:marble, stone, wood, nil];
    item1.materialsUsed = [NSArray arrayWithObjects: @"5", @"2", @"3", nil];	
    
    //materialsArray и materialsUsed нужно будет объединить в одно свойство (массив) (для каждого item), создав новый класс, и поместив в массив экземлпяры этого массива
    
    item2.header = @"Ресторан";
    item2.materials = @"Мрамор | Камень";
    item2.imageName = @"Restaurant";
//    item2.bigImage = @"Big photo";
    item2.textOfItem = @"no text";
    item2.materialsArray = [NSArray arrayWithObjects:marble, stone, nil];
    item2.materialsUsed = [NSArray arrayWithObjects: @"0", @"0", nil];
    
    item3.header = @"Приемная зона в отеле";
    item3.materials = @"Мрамор | Камень";
    item3.imageName = @"Priemnaya";
//    item3.bigImage = @"Big photo";
    item3.textOfItem =  @"no text";
    item3.materialsArray = [NSArray arrayWithObjects:marble, stone, nil];
    item3.materialsUsed = [NSArray arrayWithObjects: @"0", @"0", nil];
    
    item4.header = @"Гостиная в загородном доме";
    item4.materials = @"Мрамор | Камень | Дерево";
    item4.imageName = @"Gostinnaya";
//    item4.bigImage = @"Big photo";
    item4.textOfItem = @"no text";
    item4.materialsArray = [NSArray arrayWithObjects:marble, stone, wood, nil];
    item4.materialsUsed = [NSArray arrayWithObjects: @"0", @"0", @"0", nil];
    
    item5.header = @"Ванная комната в отеле";
    item5.materials = @"Мрамор | Камень";
    item5.imageName = @"Vannaya";
//    item5.bigImage = @"Big photo";
    item5.textOfItem = @"no text";
    item5.materialsArray = [NSArray arrayWithObjects:marble, stone, nil];
    item5.materialsUsed = [NSArray arrayWithObjects: @"0", @"0", nil];
    
    item6.header = @"Зал ожидания в аэропорту";
    item6.materials = @"Мрамор";
    item6.imageName = @"Zal ojidania";
//    item6.bigImage = @"Big photo";
    item6.textOfItem = @"no text";
    item6.materialsArray = [NSArray arrayWithObjects:marble, nil];
    item6.materialsUsed = [NSArray arrayWithObjects: @"0", nil];
    
    NSArray* dataArray = [NSArray arrayWithObjects:item1, item2, item3, item4, item5, item6, nil];
    
    return dataArray;
}



@end
