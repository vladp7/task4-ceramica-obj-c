//
//  PortfolioMenuItem.h
//  Ceramica
//
//  Created by Владислав on 20.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PortfolioMenuItem : NSObject

@property (strong, nonatomic) NSString* header;
@property (strong, nonatomic) NSString* materials;
@property (strong, nonatomic) NSString* imageName;
@property (strong, nonatomic) NSString* bigImage;
@property (strong, nonatomic) NSString* textOfItem;
@property (strong, nonatomic) NSArray* materialsArray;
@property (strong, nonatomic) NSArray* materialsUsed;

@end
